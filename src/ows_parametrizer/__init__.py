from qgis.gui import QgisInterface

# noinspection PyPep8Naming
def classFactory(iface: QgisInterface):  # pylint: disable=invalid-name
    from .owsparametrizer.owsparametrizer import OwsParametrizer
    return OwsParametrizer(iface)
