import re

from qgis.utils import iface


def fix_header(uri):
    params_to_fix = [
        "IgnoreGetFeatureInfoUrl",
        "IgnoreGetMapUrl"
    ]
    result = uri
    for item in params_to_fix:
        if item in result:
            result = re.sub("{}=0".format(item), "{}=1".format(item), result)
        else:
            result = "{}=1&{}".format(item, result)
    return result
    
    
def update_query_parameter(uri, key, value):
    ampersand = "%26"
    result = re.sub(r"{}=[\w: ]*".format(key), "", uri)
    if result.endswith(ampersand) or result.endswith("?"):
        separator = ""
    elif "?" in result:
        separator = ampersand
    else:
        separator = "?"
    return "{}{}{}={}".format(result, separator, key, value)
    
        
def update_style(uri, style=""):
    return re.sub(
        r"styles[\w:=]*&", 
        "styles={}&".format(style), 
        uri
    )
    

def get_request_uri(
        original_uri, 
        time=None, 
        style=None, 
        ship_type=None, 
        area=None, 
        frequency=None
):
    uri = fix_header(original_uri)
    if time is not None:
        uri = update_query_parameter(uri, "TIME", time)
    dimensions = {
        "DIM_SHIPTYPERANGE": ship_type,
        "DIM_AREA": area,
        "DIM_TEMPORALFREQUENCY": frequency
    }
    for dim_name, value in dimensions.items():
        if value is not None:
            uri = update_query_parameter(uri, dim_name, value)
    if style is not None:
        uri = update_style(uri, style)
    return uri

        
def update_layer_uri(style=None, time=None, ship_type=None, area=None, frequency=None):
    layer = iface.activeLayer()
    provider = layer.dataProvider()
    uri = get_request_uri(
        provider.dataSourceUri(), 
        time=time, 
        style=style, 
        ship_type=ship_type, 
        area=area, 
        frequency=frequency
    )
    provider.setDataSourceUri(uri)
    provider.reloadData()
    provider.dataChanged.emit()
