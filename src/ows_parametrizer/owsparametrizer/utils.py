import functools

import qgis.utils

INFO = qgis.core.Qgis.Info
WARNING = qgis.core.Qgis.Warning

logger = functools.partial(
    qgis.utils.QgsMessageLog.logMessage, tag=__name__, level=INFO)

