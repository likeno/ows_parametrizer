import enum

from PyQt5 import QtCore as qtcore
from PyQt5 import QtGui as qtgui
from PyQt5 import QtWidgets as qtwidgets

from qgis.utils import iface


class ExtentType(enum.Enum):
    SINGLE_VALUE = 0
    MULTIPLE_VALUES = 1
    INTERVAL = 2
    MULTIPLE_INTERVALS = 3


class ParametrizerWidget(enum.Enum):
    DATETIME_EDIT = qtwidgets.QDateTimeEdit
    INTEGER_SPINBOX = qtwidgets.QSpinBox
    SHORT_SELECTION = qtwidgets.QListView
    FLOAT_SPINBOX = qtwidgets.QDoubleSpinBox
    LINE_TEXT = qtwidgets.QLineEdit


class DimensionType(enum.Enum):
    TIME = "time"
    ELEVATION = "elevation"
    CUSTOM = "custom"
    

class ParametrizerModelIndex(enum.Enum):
    STYLES = 0
    DIMENSIONS = 1
    ADDITIONAL_PARAMETERS = 2
    FILTERS = 3


def parse_extent(raw_extent: str):
    """Parse the extent text returned from a Capabilities response"""
    interval_separator = "/"
    multiple_separator = ","
    is_interval = interval_separator in raw_extent
    is_multiple = multiple_separator in raw_extent
    result = {}
    if is_interval and is_multiple:  # list of intervals
        result["type"] = ExtentType.MULTIPLE_INTERVALS
        result["values"] = []
        for raw_interval in raw_extent.split(multiple_separator):
            interval_params = raw_interval.split(interval_separator)
            minimum, maximum, resolution = [i.strip() for i in interval_params]
            result["values"].append({
                "minimum": minimum,
                "maximum": maximum,
                "resolution": resolution,
            })
    elif is_interval:  # single extent, defining an interval
        result["type"] = ExtentType.INTERVAL
        interval_params = raw_extent.split(interval_separator)
        try:
            minimum, maximum, resolution = [i.strip() for i in interval_params]
        except ValueError:
            # FIXME: workaround for incorrect servers which do not report resolution
            minimum, maximum = [i.strip() for i in interval_params]
            resolution = 0
        result["values"] = [{
            "minimum": minimum,
            "maximum": maximum,
            "resolution": resolution,
        }]
    elif is_multiple:  # a list of multiple values
        result["type"] = ExtentType.MULTIPLE_VALUES
        result["values"] = [
            i.strip() for i in raw_extent.split(multiple_separator)]
    else:  # single value
        result["type"] = ExtentType.SINGLE_VALUE
        result["values"] = [raw_extent.strip()]
    return result


class OwsDimension(object):
    name = ""
    units = ""
    unit_symbol = None
    default = None
    multiple_values = False
    nearest_value = False
    current = False
    extent = ""
    enabled = False
    widget_type = None
    type_ = None

    def __init__(self, name, units, extent, unit_symbol=None, default=None,
                 multiple_values=False, nearest_value=False, current=False,
                 enabled=False, widget_type=None, current_value=None):
        self.name = name.lower()
        self.type_ = {
            "time": DimensionType.TIME,
            "elevation": DimensionType.ELEVATION,
        }.get(self.name, DimensionType.CUSTOM)
        self.units = units
        self.extent = extent or []
        self.unit_symbol = unit_symbol
        self.default = default
        self.multiple_values = multiple_values
        self.nearest_value = nearest_value
        # according to WMS 1.3.0, current only applies to the `time` dimension
        self.current = current if self.name == "time" else False
        self.enabled = enabled
        self.widget_type = widget_type or ParametrizerWidget.LINE_TEXT
        self.current_value = current_value or self.default

    @classmethod
    def from_xml(cls, xml_element):
        name = xml_element.attrib["name"].lower()
        return cls(
            name=name,
            units=xml_element.attrib["units"],
            extent=parse_extent(xml_element.text),
            unit_symbol = xml_element.attrib.get("unitSymbol"),
            default = xml_element.attrib.get("default"),
            multiple_values = True if xml_element.attrib.get(
                "multipleValues", "").lower() in ("true", 1) else False,
            nearest_value = True if xml_element.attrib.get(
                "nearestValue", "").lower() in ("true", 1) else False,
            current=xml_element.attrib.get("current", False),
        )

    def __str__(self):
        return f"{self.name} - {self.current_value}"


class OwsParametrizerDelegate(qtwidgets.QStyledItemDelegate):
    
    def createEditor(self, parent, option, index):
        model = index.model()
        item = model.itemFromIndex(index)
        print("item: ", item)
        print("item rows: ", item.rowCount())
        print("item columns: ", item.columnCount())
        print("item text: ", item.text())
        return super().createEditor(parent, option, index)
        

class OwsParametrizerModel(qtgui.QStandardItemModel):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        root = self.invisibleRootItem()
        for index in ParametrizerModelIndex:
            item = qtgui.QStandardItem(index.name.lower())
            item.setFlags(qtcore.Qt.ItemIsEnabled)
            root.appendRow(item)
        self.setHorizontalHeaderLabels(["parameter", "value"])
        
    def add_dimension(self, dimension):
        parent_item = self.item(ParametrizerModelIndex.DIMENSIONS.value, 0)
        dimension_item = qtgui.QStandardItem(dimension.name)
        dimension_item.appendRow([
            qtgui.QStandardItem("widget"),
            qtgui.QStandardItem(dimension.widget_type.name),
        ])
        _add_enabled_item(dimension_item, dimension.enabled)
        dimension_item.appendRow([
            qtgui.QStandardItem("current_value"),
            qtgui.QStandardItem(dimension.current_value),
        ])
        dimension_item.appendRow([
            qtgui.QStandardItem("default_value"),
            qtgui.QStandardItem(dimension.default),
        ])
        parent_item.appendRow(dimension_item)
        
    def add_style(self, style_name, enabled: bool=False):
        style_item = qtgui.QStandardItem(style_name)
        _add_enabled_item(style_item, enabled)
        parent_item = self.item(ParametrizerModelIndex.STYLES.value, 0)
        parent_item.appendRow(style_item)

    def add_filter(self):
        raise NotImplementedError

    def add_additional_parameter(self):
        raise NotImplementedError

        
def _add_enabled_item(parent_item, enabled: bool):
    enabled_item = qtgui.QStandardItem()
    enabled_item.setCheckable(True)
    enabled_item.setCheckState(
        qtcore.Qt.Checked if enabled else qtcore.Qt.Unchecked)
    parent_item.appendRow([
        qtgui.QStandardItem("enabled"),
        enabled_item,
    ])


def create_model(
        styles=None, 
        dimensions=None,
        additional_parameters=None,
        filters=None,
) -> qtgui.QStandardItemModel:
    model = OwsParametrizerModel()
    empty = []
    for dimension in dimensions or empty:
        model.add_dimension(dimension)
    for style in styles or empty:
        model.add_style(style)
    for additional_param in additional_parameters or empty:
        model.add_additional_parameter()
    for filter_ in filters or empty:
        model.add_filter()
    return model
   
    
def get_dialog(model):
    dialog = qtwidgets.QDialog(parent=iface.mainWindow())
    layout = qtwidgets.QHBoxLayout()
    tree_view = qtwidgets.QTreeView()
    tree_view.setModel(model)
    tree_view.setItemDelegate(OwsParametrizerDelegate())
    layout.addWidget(tree_view)
    dialog.setLayout(layout)
    return dialog
