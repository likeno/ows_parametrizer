import pathlib

from PyQt5 import uic
from PyQt5 import QtWidgets

UI_DIR = pathlib.Path(__file__).parent.parent / "ui"
FORM_CLASS, _ = uic.loadUiType(
    str(UI_DIR / 'owsparametrizer_dialog_base.ui'))


class OwsParametrizerDialog(QtWidgets.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(OwsParametrizerDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
