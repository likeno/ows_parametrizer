import functools
import xml.etree.ElementTree as etree

from PyQt5 import QtCore as qtcore
from PyQt5 import QtNetwork as qtnetwork
import qgis.core

from . import models
from .utils import logger
from .utils import INFO
from .utils import WARNING


def get_uri_parameters(datasource_uri: str):
    """Convert datasource_uri into a dict"""
    params = {}
    for pair in datasource_uri.split("&"):
        key, value = pair.partition("=")[::2]
        params[key] = value if value != "" else None
    return params


class OwsCapabilitiesInspector(qtcore.QObject):

    NSMAP = {
        "wms": "http://www.opengis.net/wms"
    }

    capabilities_analyzed = qtcore.pyqtSignal(
        qgis.core.QgsMapLayer,
        list
    )

    def get_capabilities(self, layer):
        provider = layer.dataProvider()
        uri_params = get_uri_parameters(provider.dataSourceUri())
        query_params = {
            "service": provider.name().upper(),
            "request": "GetCapabilities"
        }
        url_query = qtcore.QUrlQuery()
        url_query.setQueryItems([(k, v) for k, v in query_params.items()])
        request_url = qtcore.QUrl(uri_params["url"])
        request_url.setQuery(url_query)
        logger("request_url {}".format(request_url.toString()))
        request = qtnetwork.QNetworkRequest(request_url)
        # the below QNetworkRequest attribute should really be
        # QNetworkRequest.RedirectPolicyAttribute with a value of
        # QNetworkRequest.NoLessSafeRedirectPolicy
        # however, even though it was introduced in Qt5.9, it is not present in
        # PyQt5.9.1, which is what we are targeting
        # - so we fallback to QNetworkRequest.FollowRedirectsAttribute
        request.setAttribute(
            qtnetwork.QNetworkRequest.FollowRedirectsAttribute,
            True
        )
        network_access_manager = qgis.core.QgsNetworkAccessManager.instance()
        auth_id = uri_params.get("authcfg")
        if auth_id is not None:
            qgis_application = qgis.core.QgsApplication.instance()
            auth_manager = qgis_application.authManager()
            auth_manager.updateNetworkRequest(request, auth_id)
            reply = network_access_manager.get(request)
            auth_manager.updateNetworkReply(reply, auth_id)
        else:
            reply = network_access_manager.get(request)
        handler = functools.partial(self.slot_capabilities_received, layer)
        reply.finished.connect(handler)

    def slot_capabilities_received(self, layer):
        """Parse the capabilities response and extract dimensions and styles

        According to the WMS 1.3 specification:

        - dimensions set in a parent layer are inherited by child layers
          (section 7.2.4.6.10)
        - dimensions set in a child layer override parent layer dimensions
          with the same name (section 7.2.4.6.10)
        - The optional <Dimension> element is used in service metadata to
          declare that one or more dimensional parameters are relevant to a
          layer or group of layers (Annex C.2)
        - Dimension names shall be interpreted case-insensitively and should
          contain no whitespace (Annex C.2)
        """
        reply = self.sender()
        logger("capabilities received")
        http_status_code = reply.attribute(
            qtnetwork.QNetworkRequest.HttpStatusCodeAttribute)
        logger("http_status_code: {}".format(http_status_code))
        if http_status_code == 200:
            xml_tree = etree.parse(reply)
            root = xml_tree.getroot()
            # check https://stackoverflow.com/a/20132342
            parent_map = {c: p for p in root.iter() for c in p}
            named_layer_elements = root.findall(
                ".//wms:Layer/wms:Name/..", namespaces=self.NSMAP)
            logger("named_layer_elements: {}".format([el.tag for el in named_layer_elements]))
            layer_element = self.get_layer_element(layer, named_layer_elements)
            logger("layer element: {}".format(layer_element))
            if layer_element is not None:
                dimensions = self.extract_dimensions(layer_element, parent_map)
                logger("about to emit the capabilities_analyzed signal")
                self.capabilities_analyzed.emit(layer, dimensions)
            else:
                logger(
                    "Could not the corresponding layer element from "
                    "capabilities document",
                    level=WARNING
                )
        else:
            logger("Received invalid response from the server", level=WARNING)
        reply.deleteLater()

    def extract_dimensions(
            self,
            layer_element: etree.Element,
            parent_map
    ):
        """Look for Dimension definitions in the layer and its ancestors"""
        layer_lineage = self.get_layer_lineage(layer_element, parent_map)
        layer_lineage.reverse()
        logger("layer_lineage: {}".format([i.tag for i in layer_lineage]))
        result = {}
        for element in layer_lineage:
            dimension_elements = element.findall(
                "./wms:Dimension", namespaces=self.NSMAP)
            for dimension_element in dimension_elements:
                dimension = models.OwsDimension.from_xml(dimension_element)
                result[dimension.name] = dimension
        return list(result.values())

    def get_layer_element(self, layer: qgis.core.QgsMapLayer, layer_elements):
        """Get the xml layer element that refers to the input qgis map layer"""
        uri_params = get_uri_parameters(layer.dataProvider().dataSourceUri())
        for element in layer_elements:
            name = element.findall("./wms:Name", namespaces=self.NSMAP)[0].text
            logger("name: {}".format(name))
            logger("uri_params['layers']: {}".format(uri_params["layers"]))
            if name == uri_params["layers"]:
                result = element
                break
        else:
            result = None
        return result

    def get_layer_lineage(self, layer_element, parent_map):
        result = [layer_element]
        parent = parent_map[layer_element]
        if parent.tag == "{{{}}}Layer".format(self.NSMAP["wms"]):
            ancestors = self.get_layer_lineage(parent, parent_map)
            result.extend(ancestors)
        return result


