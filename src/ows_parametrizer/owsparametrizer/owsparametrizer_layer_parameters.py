import pathlib

from PyQt5 import uic
from PyQt5 import QtWidgets as qtwidgets
import qgis.gui

UI_DIR = pathlib.Path(__file__).parents[1] / "ui"
UI_MODULE_PATH = UI_DIR / "owsparametrizer_layer_parameters.ui"
FORM_CLASS, _ = uic.loadUiType(str(UI_MODULE_PATH))


class OwsParametrizerLayerParametersWidget(qtwidgets.QDialog, FORM_CLASS):

    def __init__(self, layer, canvas, parent=None):
        print("Inside OwsParametrizerMapLayerConfigWidget __init__")
        super().__init__(parent=parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self.layer = layer
        self.canvas = canvas

    def apply(self):
        return None

