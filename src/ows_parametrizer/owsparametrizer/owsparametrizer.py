"""OWS parametrizer main module

Load this inside QGIS for debugging with the following:

>>> PluginClass = qgis.utils.plugins["ows_parametrizer"]
>>> import sys
>>> plugin_module = sys.modules[PluginClass.__module__]

"""
import functools
import os.path
import time

from PyQt5 import QtCore as qtcore
from PyQt5 import QtGui as qtgui
from PyQt5 import QtWidgets as qtwidgets
import qgis.gui
import qgis.utils
import qgis.core

# Initialize Qt resources from file resources.py
from .resources import *
from .owsparametrizermaplayerconfigwidget import (
    OwsParametrizerMapLayerConfigWidget)
from .owsparametrizer_layer_parameters import (
    OwsParametrizerLayerParametersWidget)
from .layerinspector import OwsCapabilitiesInspector
from .models import create_model
from .utils import logger
from .utils import INFO
from .utils import WARNING


SUPPORTED_DATA_PROVIDERS = [
    "wms",
]


def supports_layer(map_layer):
    provider = map_layer.dataProvider()
    name = provider.name()
    result = True if name in SUPPORTED_DATA_PROVIDERS else False
    logger("supports_layer: {}".format(result), level=INFO)
    return result


class OwsParametrizer(object):

    def __init__(self, iface: qgis.gui.QgisInterface):
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = qtcore.QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'OwsParametrizer_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = qtcore.QTranslator()
            self.translator.load(locale_path)

            if qtcore.qVersion() > '4.3.3':
                qtcore.QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.widget_provider = OwsParametrizerWidgetProvider(self)
        widget_registry = qgis.gui.QgsGui.layerTreeEmbeddedWidgetRegistry()
        widget_registry.addProvider(self.widget_provider)
        self.map_layer_config_widget_factory = (
            OwsParametrizerMapLayerConfigWidgetFactory()
        )
        self.capabilities_inspector = OwsCapabilitiesInspector()
        self.capabilities_inspector.capabilities_analyzed.connect(
            self.slot_add_layer)
        self.layers = {}
        qgis_project = qgis.core.QgsProject.instance()
        qgis_project.layerWasAdded.connect(self.inspect_layer)
        for layer in qgis_project.mapLayers().values():
            self.inspect_layer(layer)

    def inspect_layer(self, layer):
        logger("Inspecting layer {}".format(layer.id()))
        if not supports_layer(layer):
            logger("Layer {} is not supported".format(layer.id()))
            return
        self.capabilities_inspector.get_capabilities(layer)

    def slot_add_layer(
            self,
            layer: qgis.core.QgsMapLayer, dimensions
    ):
        logger("slot_add_layer called with layer {}".format(layer.id()))
        self.layers[layer.id()] = create_model(dimensions=dimensions)
        embedded_widgets_key = "embeddedWidgets"
        try:
            widget_id = int(
                layer.customProperty("{}/count".format(embedded_widgets_key)))
        except TypeError:
            widget_id = 0
        layer.setCustomProperty(
            "{}/count".format(embedded_widgets_key),
            widget_id + 1
        )
        layer.setCustomProperty(
            "{}/{}/id".format(embedded_widgets_key, widget_id),
            self.widget_provider.id()
        )
        self.iface.layerTreeView().refreshLayerSymbology(layer.id())

    def slot_show_parametrizer_dialog(self, layer: qgis.core.QgsMapLayer):
        dlg = OwsParametrizerLayerParametersWidget(
            layer=layer,
            canvas=self.iface.mapCanvas(),
            parent=self.iface.mainWindow()
        )
        model = self.layers.get(layer.id())
        dlg.tree_view.setModel(model)
        dlg.exec_()

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return qtcore.QCoreApplication.translate('OwsParametrizer', message)

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        logger("running initGui")
        self.iface.registerMapLayerConfigWidgetFactory(
            self.map_layer_config_widget_factory)


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        widget_registry = qgis.gui.QgsGui.layerTreeEmbeddedWidgetRegistry()
        widget_registry.removeProvider(self.widget_provider.id())
        self.iface.unregisterMapLayerConfigWidgetFactory(
            self.map_layer_config_widget_factory)
        qgis_project = qgis.core.QgsProject.instance()
        qgis_project.layerWillBeRemoved.connect(self.remove_layer)

    def remove_layer(self, layer_id: str):
        logger("About to remove layer {}".format(layer_id))
        del self.layers[layer_id]


    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            pass


class OwsParametrizerWidgetProvider(
        qgis.gui.QgsLayerTreeEmbeddedWidgetProvider):

    def __init__(self, parent_plugin, *args, **kwargs):
        logger("Instantiating provider...")
        super().__init__(*args, **kwargs)
        self.parent_plugin = parent_plugin
        self.creation_timestamp = int(time.time())

    def id(self):
        logger("OwsParametrizerWidgetProvider.id called")
        return "{}_{}".format(self.__class__.__name__, self.creation_timestamp)

    def name(self):
        logger("OwsParametrizerWidgetProvider.name called")
        return "OWS Parameters"

    def createWidget(self, map_layer: qgis.core.QgsMapLayer, widget_index):
        # create the main widget for the plugin
        logger("OwsParametrizerWidgetProvider.createWidget called")
        tool_bar = qtwidgets.QToolBar()
        configure_action = qtwidgets.QAction(
            qtgui.QIcon(":images/themes/default/favourites"),
            "configure parameters",
            parent=self.parent_plugin.iface.mainWindow()
        )
        show_dialog_partial = functools.partial(
            self.parent_plugin.slot_show_parametrizer_dialog, map_layer)
        configure_action.triggered.connect(show_dialog_partial)
        tool_bar.addAction(configure_action)
        return tool_bar

    def supportsLayer(self, map_layer: qgis.core.QgsMapLayer):
        logger("OwsParametrizerWidgetProvider.supportsLayer called")
        return map_layer.id() in self.parent_plugin.layers.keys()


class OwsParametrizerMapLayerConfigWidgetFactory(
        qgis.gui.QgsMapLayerConfigWidgetFactory):

    _title = "OWS Parameters"
    _icon = qtgui.QIcon()
    _supports_style_dock = False
    _supports_layer_properties_dialog = True

    def createWidget(self, layer, canvas, dock_widget=True, parent=None):
        logger(
            "OwsParametrizerMapLayerConfigWidgetFactory."
            "createWidget called: {}".format(locals())
        )
        return OwsParametrizerMapLayerConfigWidget(
            layer, canvas, parent=parent)

    def icon(self):
        return self._icon

    def title(self):
        return self._title

    def supportLayerPropertiesDialog(self):
        logger(
            "OwsParametrizerMapLayerConfigWidgetFactory."
            "supportLayerPropertiesDialog "
            "called: {}".format(self._supports_layer_properties_dialog)
        )
        return self._supports_layer_properties_dialog

    def supportsLayer(self, map_layer):
        result = supports_layer(map_layer)
        logger(
            "OwsParametrizerMapLayerConfigWidgetFactory.supportsLayer "
            "called: {}".format(result)
        )
        return result

    def supportsStyleDock(self):
        logger(
            "OwsParametrizerMapLayerConfigWidgetFactory."
            "supportsStyleDock called: {}".format(self._supports_style_dock)
        )
        return self._supports_style_dock

    # def get_layer_capabilities(self, layer):
    #     provider = layer.dataProvider()
    #     uri_params = get_uri_parameters(provider.dataSourceUri())
    #     capabilities_url = qtcore.QUrl(uri_params["url"])
    #     url_query = qtcore.QUrlQuery()
    #     query_params = {
    #         "service": "WMS",
    #         "request": "GetCapabilities"
    #     }
    #     url_query.setQueryItems([(k, v) for k, v in query_params.items()])
    #     capabilities_url.setQuery(url_query)
    #     reply = self.network_access_manager.get(capabilities_url)
    #     reply.finished.connect(self.parse_layer_capabilities)
    #
    # def parse_layer_capabilities(self, reply:qtnetwork.QNetworkReply):
    #     pass
