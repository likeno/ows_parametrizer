# OWS Parametrizer

This plugin allows defining extra parameters for using with various OGC Web 
Service requests. It can be used for requesting additional WMS Dimensions, 
such as time, elevation, etc. It can be used for specifying CQL filters on 
WFS requests. It can be used to specify arbitrary query parameters.


## Development

Clone this repo, install pipenv and `pipenv install` it!


### Deploying

Call the `deploy` task:

```
pipenv run paver deploy
```